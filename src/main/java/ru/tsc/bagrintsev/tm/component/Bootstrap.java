package ru.tsc.bagrintsev.tm.component;

import ru.tsc.bagrintsev.tm.api.controller.*;
import ru.tsc.bagrintsev.tm.api.repository.*;
import ru.tsc.bagrintsev.tm.api.sevice.*;
import ru.tsc.bagrintsev.tm.controller.*;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.execution.*;
import ru.tsc.bagrintsev.tm.repository.*;
import ru.tsc.bagrintsev.tm.service.*;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;

import static ru.tsc.bagrintsev.tm.constant.CommandLineConst.*;
import static ru.tsc.bagrintsev.tm.constant.InteractionConst.*;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    public void run(final String[] args) throws IOException {
        try {
            processOnStart(args);
        } catch (AbstractException e) {
            System.err.println(e.getMessage());
        }
        commandController.showWelcome();
        try {
            initData();
        } catch (AbstractException e) {
            System.err.println("Data initialization error...");
            System.err.println(e.getMessage());
        }
        while (true) {
            try {
                System.out.println();
                System.out.println("Enter Command:");
                System.out.print(">> ");
                final String command = TerminalUtil.nextLine();
                processOnTheGo(command);
                System.out.println("[OK]");
            } catch (Exception e) {
                if (!(e instanceof AbstractException)) {
                    System.err.print(e.getClass().getSimpleName() + ": ");
                }
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private void initData() throws AbstractException {
        taskService.create("first task", "task simple description");
        taskService.create("second task", "task simple description");
        taskService.create("third task", "task simple description");
        taskService.create("fourth task", "task simple description");

        projectService.create("first project", "project simple description");
        projectService.create("second project", "project simple description");
        projectService.create("third project", "project simple description");
        projectService.create("fourth project", "project simple description");
    }

    private void processOnStart(final String arg) throws ArgumentNotSupportedException {
        if (arg == null || arg.isEmpty()) throw new ArgumentNotSupportedException(arg);
        switch (arg) {
            case HELP_SHORT:
                commandController.showHelp();
                break;
            case VERSION_SHORT:
                commandController.showVersion();
                break;
            case ABOUT_SHORT:
                commandController.showAbout();
                break;
            case INFO_SHORT:
                commandController.showSystemInfo();
                break;
            case ARGUMENTS_SHORT:
                commandController.showArguments();
                break;
            case COMMANDS_SHORT:
                commandController.showCommands();
                break;
            default:
                throw new ArgumentNotSupportedException(arg);
        }
    }

    private void processOnTheGo(final String command) throws IOException, AbstractException {
        if (command == null || command.isEmpty()) throw new CommandNotSupportedException(command);
        switch (command) {
            case HELP:
                commandController.showHelp();
                break;
            case VERSION:
                commandController.showVersion();
                break;
            case ABOUT:
                commandController.showAbout();
                break;
            case INFO:
                commandController.showSystemInfo();
                break;
            case EXIT:
                commandController.close();
                break;
            case ARGUMENTS:
                commandController.showArguments();
                break;
            case COMMANDS:
                commandController.showCommands();
                break;
            case TASK_LIST:
                taskController.showTaskList();
                break;
            case TASK_CREATE:
                taskController.createTask();
                break;
            case TASK_CLEAR:
                taskController.clearTasks();
                break;
            case PROJECT_LIST:
                projectController.showProjectList();
                break;
            case PROJECT_CREATE:
                projectController.createProject();
                break;
            case PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TASK_LIST_BY_PROJECT_ID:
                taskController.showTaskListByProjectId();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    private void processOnStart(final String[] args) throws ArgumentNotSupportedException {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        processOnStart(arg);
        commandController.close();
    }

}
