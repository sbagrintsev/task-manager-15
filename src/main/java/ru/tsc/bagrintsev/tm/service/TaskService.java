package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.repository.ITaskRepository;
import ru.tsc.bagrintsev.tm.api.sevice.ITaskService;
import ru.tsc.bagrintsev.tm.comparator.DateCreatedComparator;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;


    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(final String name) throws NameIsEmptyException {
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        return taskRepository.create(name);
    }

    @Override
    public Task create(final String name, String description) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionIsEmptyException();
        return taskRepository.create(name, description);
    }

    @Override
    public Task add(final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.add(task);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) {
            return taskRepository.findAll(DateCreatedComparator.INSTANCE);
        }
        return taskRepository.findAll(sort.getComparator());
    }

    @Override
    public Map<Integer, Task> findAllByProjectId(final String projectId) throws IdIsEmptyException {
        if (projectId == null || projectId.isEmpty()) throw new IdIsEmptyException();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task findOneByIndex(final Integer index) throws IncorrectIndexException {
        if (index == null ||
                index < 0 ||
                index > taskRepository.taskCount()) throw new IncorrectIndexException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneById(final String id) throws IdIsEmptyException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        return taskRepository.findOneById(id);
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) throws AbstractException {
        if (index == null || index < 0 || index > taskRepository.taskCount()) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        if (name == null || name.isEmpty()) throw new NameIsEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeTaskByIndex(final Integer index) throws AbstractException {
        if (index == null ||
                index < 0 ||
                index > taskRepository.taskCount()) throw new IncorrectIndexException();
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task removeTaskById(final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        return taskRepository.removeById(id);
    }

    @Override
    public Task remove(final Task task) throws TaskNotFoundException {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.remove(task);
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) throws AbstractException {
        if (index == null ||
                index < 0 ||
                index > taskRepository.taskCount()) throw new IncorrectIndexException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            task.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            task.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdIsEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) {
            task.setDateStarted(new Date());
        } else if (status.equals(Status.COMPLETED)) {
            task.setDateFinished(new Date());
        } else if (status.equals(Status.NOT_STARTED)) {
            task.setDateStarted(null);
            task.setDateFinished(null);
        }
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }
}
