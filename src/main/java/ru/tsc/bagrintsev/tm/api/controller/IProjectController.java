package ru.tsc.bagrintsev.tm.api.controller;

import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.field.*;

import java.io.IOException;

public interface IProjectController {

    void showProjectList() throws IOException, IncorrectStatusException;

    void clearProjects();

    void createProject() throws IOException, AbstractException;

    void removeProjectByIndex() throws IOException, AbstractException;

    void removeProjectById() throws IOException, AbstractException;

    void showProjectByIndex() throws IOException, IncorrectIndexException;

    void showProjectById() throws IOException, AbstractException;

    void updateProjectByIndex() throws IOException, AbstractException;

    void updateProjectById() throws IOException, AbstractException;

    void changeProjectStatusByIndex() throws IOException, AbstractException;

    void changeProjectStatusById() throws IOException, AbstractException;

    void startProjectByIndex() throws IOException, AbstractException;

    void startProjectById() throws IOException, AbstractException;

    void completeProjectByIndex() throws IOException, AbstractException;

    void completeProjectById() throws IOException, AbstractException;

}
