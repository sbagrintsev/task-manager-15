package ru.tsc.bagrintsev.tm.api.sevice;

import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.*;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;
import java.util.Map;

public interface ITaskService {

    Task create(String name) throws NameIsEmptyException;

    Task create(String name, String description) throws NameIsEmptyException, DescriptionIsEmptyException, AbstractFieldException;

    Task add(Task task) throws TaskNotFoundException;

    List<Task> findAll();

    List<Task> findAll(Sort sort);

    Map<Integer, Task> findAllByProjectId(String projectId) throws IdIsEmptyException;

    Task findOneByIndex(Integer index) throws IncorrectIndexException;

    Task findOneById(String id) throws IdIsEmptyException;

    Task updateByIndex(Integer index, String name, String description) throws NameIsEmptyException, IncorrectIndexException, TaskNotFoundException, AbstractException;

    Task updateById(String id, String name, String description) throws NameIsEmptyException, IdIsEmptyException, TaskNotFoundException, AbstractException;

    Task removeTaskByIndex(Integer index) throws IncorrectIndexException, TaskNotFoundException, AbstractException;

    Task removeTaskById(String id) throws IdIsEmptyException, TaskNotFoundException, AbstractException;

    Task remove(Task task) throws TaskNotFoundException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws IncorrectIndexException, TaskNotFoundException, AbstractException;

    Task changeTaskStatusById(String id, Status status) throws IdIsEmptyException, TaskNotFoundException, AbstractException;

    void clear();

}
