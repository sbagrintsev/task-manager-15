package ru.tsc.bagrintsev.tm.api.controller;

import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.io.IOException;

public interface IProjectTaskController {

    void bindTaskToProject() throws IOException, AbstractException;

    void unbindTaskFromProject() throws IOException, AbstractException;

}
