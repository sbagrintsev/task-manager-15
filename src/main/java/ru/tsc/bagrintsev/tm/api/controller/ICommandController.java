package ru.tsc.bagrintsev.tm.api.controller;

public interface ICommandController {
    void showSystemInfo();

    void showWelcome();

    void showHelp();

    void showCommands();

    void showArguments();

    void showVersion();

    void showAbout();

    void close();
}
