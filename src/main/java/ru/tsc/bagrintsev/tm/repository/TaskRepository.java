package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.*;


public final class TaskRepository implements ru.tsc.bagrintsev.tm.api.repository.ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task create(final String name) {
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public Task create(final String name, final String description) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    @Override
    public Task add(final Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        final List<Task> result = new ArrayList<>(tasks);
        result.sort(comparator);
        return result;
    }

    @Override
    public Map<Integer, Task> findAllByProjectId(final String projectId) {
        final Map<Integer, Task> taskMap = new TreeMap<>();
        for (int taskIndex = 0; taskIndex < taskCount(); taskIndex++) {
            final Task task = tasks.get(taskIndex);
            final String prjId = task.getProjectId();
            if (prjId == null) continue;
            if (prjId.equals(projectId)) {
                taskMap.put(taskIndex, task);
            }
        }
        return taskMap;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return tasks.get(index);
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : tasks) {
            if (task.getId().equals(id)) {
                return task;
            }
        }
        return null;
    }

    @Override
    public Task remove(final Task task) {
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) throws TaskNotFoundException {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Override
    public Task removeById(final String id) throws TaskNotFoundException {
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        return remove(task);
    }

    @Override
    public int taskCount() {
        return tasks.size();
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void setProjectId(final String taskId, final String projectId) throws TaskNotFoundException {
        final Task task = findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void clear() {
        tasks.clear();
    }
}
