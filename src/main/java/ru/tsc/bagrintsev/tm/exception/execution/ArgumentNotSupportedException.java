package ru.tsc.bagrintsev.tm.exception.execution;

import ru.tsc.bagrintsev.tm.exception.AbstractException;

public final class ArgumentNotSupportedException extends AbstractException {

    public ArgumentNotSupportedException(String arg) {
        super(String.format("Error! Argument '%s' is not supported...\n", arg));
    }

}
