package ru.tsc.bagrintsev.tm.exception.field;

public final class DescriptionIsEmptyException extends AbstractFieldException {

    public DescriptionIsEmptyException() {
        super("Error! Description is empty...");
    }

}
