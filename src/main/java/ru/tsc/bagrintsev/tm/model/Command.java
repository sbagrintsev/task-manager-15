package ru.tsc.bagrintsev.tm.model;

import static ru.tsc.bagrintsev.tm.constant.CommonConst.*;

public final class Command {

    private String argumentName;

    private String commandName;

    private String description;

    public Command(final String argumentName, final String commandName, final String description) {
        this.argumentName = argumentName == null ? EMPTY : argumentName;
        this.commandName = commandName == null ? EMPTY : commandName;
        this.description = description == null ? EMPTY : description;
    }

    public String getArgumentName() {
        return argumentName;
    }

    public void setArgumentName(final String argumentName) {
        this.argumentName = argumentName == null ? EMPTY : argumentName;
    }

    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(final String commandName) {
        this.commandName = commandName == null ? EMPTY : commandName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description == null ? EMPTY : description;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        result.append("[");
        result.append(String.format("%-25s", argumentName));
        result.append(" | ");
        result.append(String.format("%-35s", commandName));
        result.append("]");
        if (!description.isEmpty()) {
            while (result.length() < 75) {
                result.append(" ");
            }
            result.append(description);
        }

        return result.toString();
    }

}
