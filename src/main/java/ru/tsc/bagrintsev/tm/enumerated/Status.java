package ru.tsc.bagrintsev.tm.enumerated;

import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    private final String displayName;

    public static Status toStatus(final String value) throws IncorrectStatusException {
        if (value == null || value.isEmpty()) throw new IncorrectStatusException();
        for (Status status : Status.values()) {
            if (status.toString().equals(value)) {
                return status;
            }
        }
        throw new IncorrectStatusException();
    }

    public static String toName(final Status status) {
        if (status == null) return "";
        for (Status value : Status.values()) {
            if (value.equals(status)) {
                return status.getDisplayName();
            }
        }
        return null;
    }

    Status(final String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
