package ru.tsc.bagrintsev.tm.enumerated;

import ru.tsc.bagrintsev.tm.comparator.DateCreatedComparator;
import ru.tsc.bagrintsev.tm.comparator.DateStartedComparator;
import ru.tsc.bagrintsev.tm.comparator.NameComparator;
import ru.tsc.bagrintsev.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum Sort {

    BY_STATUS("Sort by status", StatusComparator.INSTANCE),
    BY_NAME("Sort by name", NameComparator.INSTANCE),
    BY_CREATED("Sort by date created", DateCreatedComparator.INSTANCE),
    BY_STARTED("Sort by date started", DateStartedComparator.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    Sort(final String displayName, final Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public static Sort toSort(final String value) {
        if (value == null || value.isEmpty()) return Sort.BY_CREATED;
        for (Sort sort : Sort.values()) {
            if (sort.toString().equals(value)) {
                return sort;
            }
        }
        return null;
    }

}
